#include <iostream>
#include <thread>
#include <mutex>
#include <cstdlib>
#include <chrono>


class ChopsticksManager {
public:
    void getChopsticks() {
        m.lock();
    }

    void putChopsticks() {
        m.unlock();
    }

private:
    std::mutex m;
};


class Philosopher {
public:
    explicit Philosopher(ChopsticksManager &cm_)
    : cm(cm_) 
    {
    }

    void live() {
        for (;;) {
            eatSushi();
            think();
        }
    }

private:
    ChopsticksManager &cm;

    void eatSushi() {
        std::cout << "Philosoper " << std::this_thread::get_id() << " wants sushi\n";
        cm.getChopsticks();
        std::cout << "Philosoper " << std::this_thread::get_id() << " starts eating sushi\n";
        _wait();
        cm.putChopsticks();
        std::cout << "Philosoper " << std::this_thread::get_id() << " done eating\n";
    }

    void think() {
        std::cout << "Philosoper " << std::this_thread::get_id() << " starts thinging...\n";
        _wait();
    }

    static void _wait() {
        std::srand(std::time(NULL));
        std::this_thread::sleep_for(std::chrono::milliseconds(
            int(float(rand()) / RAND_MAX * 1000 + 1000)
        ));
    }
};


void runPhilosopher(Philosopher p) {
    p.live();
}


int main() {
    ChopsticksManager cm;

    Philosopher KourtneyKardashian(cm);
    Philosopher KimKardashian(cm);
    Philosopher KhloeKardashian(cm);
    Philosopher RobKardashian(cm);
    Philosopher KendalJenner(cm);

    std::thread t0(runPhilosopher, KourtneyKardashian);
    std::thread t1(runPhilosopher, KimKardashian);
    std::thread t2(runPhilosopher, KhloeKardashian);
    std::thread t3(runPhilosopher, RobKardashian);
    std::thread t4(runPhilosopher, KendalJenner);

    t0.join();
    t1.join();
    t2.join();
    t3.join();
t4.join();

    return 0;
}